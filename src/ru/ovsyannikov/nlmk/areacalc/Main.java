package ru.ovsyannikov.nlmk.areacalc;

import ru.ovsyannikov.nlmk.areacalc.shapes.Circle;
import ru.ovsyannikov.nlmk.areacalc.shapes.Rectangle;
import ru.ovsyannikov.nlmk.areacalc.shapes.Square;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Main {
    public static void main(String[] args) {
        Shape square1 = new Square(2);
        Shape square2 = new Square(10);
        Shape square3 = new Square(7);

        Shape circle1 = new Circle(1);
        Shape circle2 = new Circle(15);
        Shape circle3 = new Circle(7);

        Shape rectangle1 = new Rectangle(1,2);
        Shape rectangle2 = new Rectangle(7,11);
        Shape rectangle3 = new Rectangle(12,10);



        List<Shape> shapes = new ArrayList<>();
        addToList(shapes,rectangle1,circle1,square1);
        System.out.println("Area of shapes = " + getArea(shapes));

        List<Square> squares = new ArrayList<>();
        addToList((List<Shape>) (List<?>) squares,square1,square2,square3);
        System.out.println("Area of squares = " + getArea(squares));

        List<Circle> circles = new ArrayList<Circle>();
        addToList((List<Shape>) (List<?>) circles,circle1,circle2,circle3);
        System.out.println("Area of circles = " + getArea(circles));

        List<Rectangle> rectangles = new ArrayList<>();
        addToList((List<Shape>) (List<?>) rectangles,rectangle1,rectangle2,rectangle3);
        System.out.println("Area of rectangles = " + getArea(rectangles));
    }

    public static void addToList(List<? super Shape> items, Shape...shapes){
        items.addAll(Arrays.asList(shapes));
    }

    public static double getArea(List<? extends Shape> items){
        double result = 0;
        for (Shape shape: items){
            result += ((Shape) shape).getArea();
        }
        return Math.round(result);
    }

}