package ru.ovsyannikov.nlmk.areacalc;

public abstract class Shape {
    public abstract double getArea();

}
