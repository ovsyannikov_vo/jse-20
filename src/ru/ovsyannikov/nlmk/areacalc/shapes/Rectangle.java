package ru.ovsyannikov.nlmk.areacalc.shapes;

import ru.ovsyannikov.nlmk.areacalc.Shape;

public class Rectangle extends Shape {

    private double width;
    private double length;

    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }

    public double getArea(){
        return length*width;
    }

}

