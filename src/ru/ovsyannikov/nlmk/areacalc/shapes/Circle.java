package ru.ovsyannikov.nlmk.areacalc.shapes;

import ru.ovsyannikov.nlmk.areacalc.Shape;

public class Circle extends Shape {
    private double length;

    public Circle(double length) {
        this.length = length;
    }

    public double getArea(){
        return Math.PI * (length * length);
    }

}

