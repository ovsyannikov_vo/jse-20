package ru.ovsyannikov.nlmk.areacalc.shapes;

import ru.ovsyannikov.nlmk.areacalc.Shape;

public class Square extends Shape {
    private double length;

    public Square(double length) {
        this.length = length;
    }

    public double getArea(){
        return length*length;
    }

}

